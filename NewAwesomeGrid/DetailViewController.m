//
//  DetailViewController.m
//  NewAwesomeGrid
//
//  Created by user on 4/4/14.
//  Copyright (c) 2014 user. All rights reserved.
//

#import "DetailViewController.h"
#import "CollectionViewController.h"
#import "Cell.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize detailText;

@synthesize scrollView_;

@synthesize allDict;

@synthesize methd;

@synthesize enabled;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bigImage.image = self.truckImage;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(Save)];

    self.scrollView_.contentSize = self.view.frame.size;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Save {
    if (self.block) {
        self.block(self);
    } else{
        [self.delegate detailViewController:self savedForCell:self.cellIndex];
    }
    /*
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Test" message:@"Working" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    [alertView show];
    */
}


-(void)viewWillAppear:(BOOL)animated {
    
    
    
    [super viewWillAppear:animated];
    
    
    NSLog(@"Registering of keyboard events");
 
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
 
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
 
    //In normal status the keyboard is hide
    keyboardVisible_ = NO;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    NSLog(@"Unregistering keyboard events");
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


#pragma mark - Keyboard Handlers    

-(void)keyboardWillShow:(NSNotification *)notif {
    
    
    
    if (keyboardVisible_) {
        NSLog(@"Keyboard is visible. ignoring Notification");
        return;
    }
    
    NSDictionary *info = [notif userInfo];
    //Reading information from dictionary
    
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    //Recieved value from dictionary
    
    CGRect keyboardRect = [aValue CGRectValue];
    
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    //Recieved upper edge of keyboard in coordinate system for alignment down edge
    
    CGRect viewFrame = self.view.bounds;
    
    viewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    //Change size
    
   self.scrollView_.frame = viewFrame;
    
    keyboardVisible_ = YES;
    
    //Updating and visible
    NSLog(@"Recieved KeyboardShowNotification");
    
    [UIView animateWithDuration:0.3f delay:0.2f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [self.detailText setContentOffset:CGPointMake(1.0, 1.0)];
    } completion:nil];

    
    
    
}

-(void)keyboardWillHide:(NSNotification *)notif {
    
    if (!keyboardVisible_) {
        NSLog(@"Keyboard is hidden. ignoring notification");
        return;
    }
    
   self.scrollView_.frame = self.view.bounds;
    keyboardVisible_ = NO;
    
    NSLog(@"Recieved KeyboardHideNotification");
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
