//
//  Cell.h
//  NewAwesomeGrid
//
//  Created by user on 4/4/14.
//  Copyright (c) 2014 user. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"


@interface Cell : UICollectionViewCell <UITextViewDelegate>  {
    
    UITextView *myText;
}



@property (weak, nonatomic) IBOutlet UIImageView *myImg;


@property (strong, nonatomic) IBOutlet UITextView *myText;

@property (weak, nonatomic) IBOutlet UILabel *descriptLabel;



@end
