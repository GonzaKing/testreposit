//
//  ViewController.h
//  NewAwesomeGrid
//
//  Created by user on 4/4/14.
//  Copyright (c) 2014 user. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DetailViewController.h"



@interface CollectionViewController: UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate, DetailNotifier>

 {
    
    NSArray *arrImg;
    
    NSArray *arrDescript;
    
    NSArray *bigImg;
     
    NSDictionary *dictionary;
    
     NSArray *arr;
}

@property (weak, nonatomic) id<UITextViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UICollectionView *myCollectView;

@property (nonatomic, strong) NSArray *arrImg;

@property (nonatomic, strong) NSArray *bigImg;
@property (nonatomic, strong) NSDictionary *dictionary;

@property (nonatomic, strong) NSArray *arr;

@property (nonatomic, strong) NSMutableArray *cellText;

@end
