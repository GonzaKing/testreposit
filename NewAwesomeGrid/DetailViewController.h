//
//  DetailViewController.h
//  NewAwesomeGrid
//
//  Created by user on 4/4/14.
//  Copyright (c) 2014 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailNotifier;

@class DetailViewController;

typedef void(^DetailSaveBlock)(DetailViewController *controller);

@interface DetailViewController : UIViewController <UITextViewDelegate> {
    
    UITextView *detailText;
    
    BOOL keyboardVisible_;
    
    UIScrollView *scrollView_;
    
    NSMutableArray *allDict;
    
    NSMutableDictionary *methd;
    
    
    
}



@property (weak, nonatomic) IBOutlet UIImageView *bigImage;

@property (strong, nonatomic) IBOutlet UITextView *detailText;

@property (weak, nonatomic) IBOutlet UILabel *labelDes;

@property (strong, nonatomic) UIImage *truckImage;

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView_;

@property (nonatomic, strong) NSMutableArray *allDict;

@property (nonatomic, strong) NSMutableDictionary *methd;

@property (nonatomic, strong) NSString *myString;

@property (nonatomic) BOOL enabled;

@property (nonatomic, copy) DetailSaveBlock block;

@property (nonatomic, weak) id<DetailNotifier> delegate;
@property (nonatomic) NSIndexPath *cellIndex;

-(void)Save;

//-(void)keyboardDidShow:(NSNotification *) notif;

//-(void)keyboardDidHide:(NSNotification *) notif;
-(void)keyboardWillShow:(NSNotification *) notif;
-(void)keyboardWillHide:(NSNotification *) notif;


@end

@protocol DetailNotifier <NSObject>

- (void)detailViewController:(DetailViewController *)controller savedForCell:(NSIndexPath *)cell;

@end