//
//  ViewController.m
//  NewAwesomeGrid
//
//  Created by user on 4/4/14.
//  Copyright (c) 2014 user. All rights reserved.
//

#import "CollectionViewController.h"
#import "Cell.h"
#import "DetailViewController.h"

@interface CollectionViewController ()

@end

@implementation CollectionViewController

@synthesize delegate;

@synthesize arrImg, bigImg;
@synthesize dictionary;
@synthesize arr;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationItem setTitle:@"Animals"];
    
    /*
     //register the class of elements
     [self.myCollectView registerClass:[Cell class]forCellWithReuseIdentifier:@"MY_CELL"];
     
     //register XIB
     [self.myCollectView registerNib:[UINib nibWithNibName:@"Cell" bundle:nil] forCellWithReuseIdentifier:@"MY_CELL"];
     
     //init the layout
     UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
     
     //set the scroll
     [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
     
     [self.myCollectView setCollectionViewLayout:flowLayout];
     
     */
    
    [self loadImg];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.myCollectView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadImg {
    
    
    
    arrImg = [NSArray arrayWithObjects:@"coala.jpeg", @"kangaroo.jpeg", @"panda.jpeg", nil];
    
        
    arrDescript = [NSArray arrayWithObjects:@"coala", @"kangaroo", @"panda", nil];
   
    bigImg = [NSArray arrayWithObjects:@"coala300.jpeg", @"kangaroo300.jpeg", @"panda300.jpeg", nil];
    
    _cellText = [NSMutableArray arrayWithObjects:@"Text",@"Text", @"Text", nil];
}


#pragma mark - Collection Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    
    return 1;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    //return numbers.count;
    return [arrImg count];
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    Cell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    
    
    //[[cell myImg]setImage:[UIImage imageNamed:[arrImg objectAtIndex:indexPath.item]]];
    
    [[cell descriptLabel] setText:[arrDescript objectAtIndex:indexPath.item]];
    
    UIImage *truckImage = [[UIImage alloc] init];
    
    truckImage = [UIImage imageNamed:[self.arrImg objectAtIndex:indexPath.row]];
    
    cell.myImg.image = truckImage;

    cell.myText.text = [self.cellText objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
   /*
    
    NSString *message = [NSString stringWithFormat:@"Working"];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Test" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    */
    
    
}
#pragma mark - Segue Method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UICollectionViewCell *cell = (UICollectionViewCell *)sender;
    
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    
    DetailViewController *dvc = (DetailViewController *)segue.destinationViewController;
    
    dvc.truckImage = [UIImage imageNamed:[self.bigImg objectAtIndex:indexPath.row]];
    dvc.cellIndex = indexPath;
//    dvc.delegate = self;
    
    dvc.block = ^(DetailViewController *controller){
        self.cellText[controller.cellIndex.row] = controller.detailText.text;
    };
}

#pragma mark - DetailDelegate
- (void)detailViewController:(DetailViewController *)controller savedForCell:(NSIndexPath *)cell
{
    self.cellText[cell.row] = controller.detailText.text;
}
@end
